# Installation
> `npm install --save @types/concat-map`

# Summary
This package contains type definitions for concat-map (https://github.com/substack/node-concat-map).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/concat-map.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/concat-map/index.d.ts)
````ts
// Type definitions for concat-map 0.0
// Project: https://github.com/substack/node-concat-map
// Definitions by: Claas Ahlrichs <https://github.com/claasahl>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.7

export = concat_map;

declare function concat_map<T, R>(xs: T[], fn: (x: T, i: number) => R | R[]): R[];

````

### Additional Details
 * Last updated: Thu, 12 Aug 2021 00:31:26 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Claas Ahlrichs](https://github.com/claasahl).
